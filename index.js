"use strict";

const Promise = require("bluebird");
const AWebappModule = require("anna-express/AWebappModule");

module.exports = class StaticPagesModule extends AWebappModule {
    constructor (core, settings) {
        super(core, settings);

        this._router = this.servePage.bind(this);

        this.requiredModules.push("anna-session");
        this.requiredModules.push("anna-postgres");
    }

    postInit () {
        super.postInit();
        this.registerModels(require("./models.js"));
    }

    servePage (req, res, next) {
        this.log.debug(`${this.constructor.name}.servePage()`);
        if ("GET" === req.method) {
            const uriToCheck = req._parsedOriginalUrl.pathname;
            this.log.debug(`uriToCheck: [${uriToCheck}]`);

            const StaticPage = this.getModel("StaticPage");
            StaticPage.getActivePageWithUrl(uriToCheck).then(page => {
                if (!!page) {
                    this.log.debug(`URL ${uriToCheck} is found, serving`);
                    const tplVars = {};
                    Object.assign(tplVars, res.locals, { page : page });
                    page.content = this.nunjucks.renderString(page.content, tplVars);

                    res.render(page.template, { page: page });
                } else {
                    this.log.debug(`URL ${uriToCheck} not found, skipping`);
                    next();
                }
            }).catch(err => {
                next(err);
            });
        } else {
            next();
        }
    }
}
