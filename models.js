"use strict";

const path = require("path");
const async = require('asyncawait/async');
const await = require('asyncawait/await');
const ADbModel = require("anna-postgres/ADbModel");

class StaticPageModel extends ADbModel {
    get table () {
        return "static_pages";
    }

    sql (filename) {
        const realFilename = path.resolve(path.join(__dirname, filename));
        return super.sql(realFilename);
    }

    getActivePageWithUrl (url) {
        return this.db.oneOrNone(this.sql("./sql/activePage.sql"), {
            url: url,
            isActive: true
        });
    }
}

module.exports = {
    StaticPage: StaticPageModel
}
